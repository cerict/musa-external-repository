#
# Cookbook Name:: mysql
# Recipe:: default
#
# Copyright 2015, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
if(!(::File.exists?("/etc/my.cnf")))

package 'mysql-community-server' do
action :install
end

package 'mysql-community-server-client' do
action :install
end

service 'mysql.service' do
action [:enable,:start]
end

execute 'pass_mysql' do
  user "root"
  command <<-EOF
	mysql --user=root -e"set password for 'root'@'localhost' = password('prova');" mysql
	EOF
end

end
