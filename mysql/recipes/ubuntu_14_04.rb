#
# Cookbook Name:: enforcement
# Recipe:: planning
#
# Copyright 2010-2015, Cerict, Napoli
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#     ----------------------------------------
#
# This product includes software developed at "Company", http://url/ .
#
# Developers:
#  * De Rosa Pasquale <pakygta@gmail.com>
#

include_recipe "enabling-platform::debconf"

# install mysql
bash "install_mysql" do
    user "root"
    cwd  "/opt"
    code <<-EOH
    sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password prova'
    sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password prova'
    sudo apt-get install -y mysql-server
    EOH
    not_if { ::File.exists?("/etc/mysql/my.cnf") }
end

# start mysql
bash "start-mysql" do
        user "root"
        cwd  "/opt"
        code <<-EOH
        sudo service mysql start
        EOH
        not_if "ps aux | grep '[m]ysqld'"
end
