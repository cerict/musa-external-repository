# SPECS Chef Repository - External dependencies

The external dependecies repository hosts the cookbooks that are not maintained by SPECS but are needed by the SPECS Cookbooks. All the cookbooks hosted under this repository are automatically uploaded 
into the Chef Server and can be referenced by any of SPECS Cookbooks.

# Repository structure

The Chef Repository contains a set of directories where all the cookbooks are described. 
Each cookbook must have its own directory and the name of the directory represents the name of the cookbook. The cookbook must be define in accordance with [Chef cookbooks guidelines](https://docs.chef.io/cookbook_repo.html)

## Default cookbook structure

* specs-core-external-repository/
  * external_cookbook_name1/
  * external_cookbook_nameN/

ATTENTION: before uploading an external cookbook to this repository make sure that the external cookbook is valid and fully working with the current SPECS Chef Server.