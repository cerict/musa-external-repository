name             'enabling-platform'
maintainer       'MUSA Project -- Layer/Group'
maintainer_email 'support@musa-project.eu'
license          'Apache 2.0'
description      'Chef Cookbook to manage the deployment of various MUSA Platform Components'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.0.1'
