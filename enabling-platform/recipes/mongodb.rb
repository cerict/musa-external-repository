#
# Copyright 2010-2015, Cerict, Napoli
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#     ----------------------------------------
#
# This product includes software developed at "Company", http://url/ .
#
# Developers:
#  * De Rosa Pasquale <pakygta@gmail.com>
#

bash "download_mongodb_public_key" do
    user "root"
    cwd  "/opt"
    code <<-EOH
    apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
    echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
    EOH
    not_if { ::File.exists?("/etc/apt/sources.list.d/mongodb-org-3.2.list") }
end

bash "install_mongodb" do
    user "root"
    cwd  "/opt"
    code <<-EOH
    sudo apt-get update
    sudo apt-get install -y mongodb-org=3.2.7 mongodb-org-server=3.2.7 mongodb-org-shell=3.2.7 mongodb-org-mongos=3.2.7 mongodb-org-tools=3.2.7
    EOH
end

bash "start-mongodb" do
        user "root"
        cwd  "/opt"
        code <<-EOH
        sudo service mongod start
        EOH
        not_if "ps aux | grep '[m]ongodb'"
end
