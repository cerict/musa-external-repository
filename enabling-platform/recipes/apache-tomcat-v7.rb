#
# Copyright 2010-2015, Cerict, Napoli
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#     ----------------------------------------
#
# This product includes software developed at "Company", http://url/ .
#
# Developers:
#  * De Rosa Pasquale <pakygta@gmail.com>
#

include_recipe "enabling-platform::jdk_v8"

bash "download_apache_tomcat_v7" do
    user "root"
    cwd  "/opt"
    code <<-EOH
    wget http://mirror.nohup.it/apache/tomcat/tomcat-7/v7.0.73/bin/apache-tomcat-7.0.73.tar.gz
    EOH
    not_if { ::File.exists?("/opt/apache-tomcat-7") }
end

# untar dbInst tar file
bash "untar_apache_tomcat_v7" do
    user "root"
    cwd  "/opt"
    code <<-EOH
    tar -zxvf /opt/apache-tomcat-7.0.73.tar.gz
    rm /opt/apache-tomcat-7.0.73.tar.gz
    mv /opt/apache-tomcat-7.0.73.tar.gz /opt/apache-tomcat-7
    EOH
    not_if { ::File.exists?("/opt/apache-tomcat-7") }
end

cookbook_file "apache-tomcat-v7-tomcat-users.xml" do
   path "/opt/apache-tomcat-7/conf/tomcat-users.xml"
   action :create
end

execute 'remove-examples-dir' do
  only_if do
    ::File.exists?("/opt/apache-tomcat-7/webapps/examples")
  end
  command 'rm -rf /opt/apache-tomcat-7/webapps/examples'
  action :run
end

bash "start-apache-tomcat-7" do
        user "root"
        cwd  "/opt/apache-tomcat-7/bin"
        code <<-EOH
        ./catalina.sh start
        EOH
        not_if "ps aux | grep '[a]pache-tomcat'"
end
