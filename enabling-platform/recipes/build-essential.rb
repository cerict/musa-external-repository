#
# Copyright 2010-2015, Cerict, Napoli
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#     ----------------------------------------
#
# This product includes software developed at "Company", http://url/ .
#
# Developers:
#  * De Rosa Pasquale <pakygta@gmail.com>
#

bash "install_build_essential" do
    user "root"
    cwd  "/opt"
    code <<-EOH
    sudo apt-get update
    sudo apt-get install -y build-essential
    touch /usr/share/doc/build-essential/installed.log
    EOH
    not_if { ::File.exists?("/usr/share/doc/build-essential/installed.log") }
end
