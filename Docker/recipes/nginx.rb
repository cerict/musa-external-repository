#
# Cookbook Name:: Docker
# Recipe:: default
#
# Copyright 2016, CeRICT - Giancarlo Capone
#
# All rights reserved - Do Not Redistribute
#

include_recipe "default"
bash 'install_nginx' do 
	user 'root'
	code <<-EOH
		docker run --name nginx -p 80:80  -d nginx		
	EOH
	not_if{"docker images | grep '[n]ginx'"}
end
