#
# Cookbook Name:: Docker
# Recipe:: default
#
# Copyright 2016, CeRICT - Giancarlo Capone
#
# All rights reserved - Do Not Redistribute
#
bash 'install_docker' do 
	user 'root'
	code <<-EOH
		apt-get install -y docker.io
		ln -sf /usr/bin/docker.io /usr/local/bin/docker
		
	EOH
	not_if { ::File.exists?("/usr/local/bin/docker") }
end
